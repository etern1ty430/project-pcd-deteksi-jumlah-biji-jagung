i=imread('im3.jpg');
bw1=rgb2gray(i);
medf=medfilt2(bw1);
se=strel('square',1);
sisi=edge(medf,'sobel');
se90 = strel('line',3,90);
se0 = strel('line',3,0);
%struktur biji jagung
BWsdil = imdilate(sisi,[se90 se0]);
BWsdil = imdilate(BWsdil,[se90 se0]);
%semua jagung
BWdfill = imfill(BWsdil,'holes');
imshow(BWsdil);
figure;
%menghilangkan border
BWnobord = imclearborder(BWdfill,4);
seD = strel('diamond',4);
BWfinal = imerode(BWnobord,seD);
BWfinal = imerode(BWfinal,seD);
BWfinal = imerode(BWfinal,seD);
BWfinal = imerode(BWfinal,seD);
BWfinal = imerode(BWfinal,seD);
BWfinal = imerode(BWfinal,seD);

seD1 = strel('disk',4);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imdilate(BWfinal,seD1);
BWfinal = imerode(BWfinal,[se90 se0]);
BWfinal = imerode(BWfinal,[se90 se0]);

%untuk diameter coin
[centers, radii,metric] = imfindcircles(BWfinal,[70 100]);
besarcoin=radii*2;
gambarkoin=imcrop(bw1,[centers(1,1)-radii centers(1,2)-radii besarcoin besarcoin]);

stats = regionprops(BWfinal);

imshow(gambarkoin);
figure;
imshowpair(i,BWfinal,'montage');

panjang="";
lebar="";
for k = 1 : length(stats)
  thisBB = stats(k).BoundingBox;
  %panjang
  rectangle('Position', [thisBB(1),thisBB(2)-10,thisBB(3),0],'EdgeColor','r','LineWidth',2 )
  if panjang==""
    panjang=num2str(thisBB(3)/68.2);
  else
    panjang=panjang+"|"+num2str(thisBB(3)/68.2);
  end
  %lebar
  rectangle('Position', [thisBB(1)-10,thisBB(2),0,thisBB(4)],'EdgeColor','r','LineWidth',2 )
  if lebar==""
    lebar=num2str(thisBB(4)/68.2);
  else
    lebar=lebar+"|"+num2str(thisBB(4)/68.2);
  end
end
stats = struct2table(stats);
ArrPStr=split(panjang,'|');
ArrPStr=double(ArrPStr);
ArrLStr=split(lebar,'|');
ArrLStr=double(ArrLStr);
for kk = 1:height(stats)
    text(stats.Centroid(kk,1)-25, stats.Centroid(kk,2)-10,'P:');
    text(stats.Centroid(kk,1), stats.Centroid(kk,2)-10,num2str(ArrPStr(kk)));

    text(stats.Centroid(kk,1)-25, stats.Centroid(kk,2)+10,'L:');
    text(stats.Centroid(kk,1), stats.Centroid(kk,2)+10,num2str(ArrLStr(kk)));
end