function varargout = proyek(varargin)
% PROYEK MATLAB code for proyek.fig
%      PROYEK, by itself, creates a new PROYEK or raises the existing
%      singleton*.
%
%      H = PROYEK returns the handle to a new PROYEK or the handle to
%      the existing singleton*.
%
%      PROYEK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROYEK.M with the given input arguments.
%
%      PROYEK('Property','Value',...) creates a new PROYEK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before proyek_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to proyek_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help proyek

% Last Modified by GUIDE v2.5 16-May-2019 09:56:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @proyek_OpeningFcn, ...
                   'gui_OutputFcn',  @proyek_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before proyek is made visible.
function proyek_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to proyek (see VARARGIN)

% Choose default command line output for proyek
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes proyek wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = proyek_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename pathname] = uigetfile({'*.jpg';'*.bmp'},'File Selector');
image=strcat(pathname,filename);
axes(handles.axes3)
imshow(image);
%baca inputan gambar 
%------------------------------------------------------------------------------------------------------------------------

i=image;
bw1=rgb2gray(i);
%------------------------------------------------------------------------------------------------------------------------

%membersihkan noise dan melakukan edge detection
%------------------------------------------------------------------------------------------------------------------------
bersih=imfilter(bw1,0.5);
bersih=imgaussfilt(bersih);
bersih=medfilt2(bersih);
sisi=edge(bersih,'log');
%------------------------------------------------------------------------------------------------------------------------

%menghilangkan sisi jagung agar mempermudah proses bounding box 
%------------------------------------------------------------------------------------------------------------------------
se90 = strel('line',3,90);
se0 = strel('line',3,0);
dilasi=imdilate(sisi,[se0 se90]);
isi=imfill(dilasi,'holes');
rangka=bwperim(isi);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
hasil=sisi-rangka;
%------------------------------------------------------------------------------------------------------------------------

%cetak bounding box untuk jagung
%------------------------------------------------------------------------------------------------------------------------
axes(handles.axes4)
imshow(image);
hasil=imbinarize(hasil);
stats=regionprops(hasil);

for k = 1 : length(stats)
  temp=stats(k).Area;
  if temp>50 && temp<105 && stats(k).BoundingBox(1)<950 
  thisBB = stats(k).BoundingBox;
  rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],'EdgeColor','r','LineWidth',1 )
  end
end
%------------------------------------------------------------------------------------------------------------------------

%properti coin dan gambar bounding box untuk coin
%------------------------------------------------------------------------------------------------------------------------
koin=rgb2gray(i);
sisikoin=edge(koin,'sobel');
sisikoin=imdilate(sisikoin,[se0 se90]);
sisikoin=imfill(sisikoin,'holes');
se=strel('diamond',4);
sisikoin=imerode(sisikoin,se);
sisikoin=imdilate(sisikoin,se);

statskoin=regionprops(sisikoin);
panjangkoin=0;
lebarkoin=0;
for k = 1 : length(statskoin)
  temp=statskoin(k).Area;
  if statskoin(k).BoundingBox(3)<200
  thisBB = statskoin(k).BoundingBox;
  rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],'EdgeColor','r','LineWidth',1 )
  panjangkoin=thisBB(3);
  lebarkoin=thisBB(4);
  end
end
%------------------------------------------------------------------------------------------------------------------------

%cetak tulisan di setiap di coin
%------------------------------------------------------------------------------------------------------------------------
statskoin = struct2table(statskoin);
for kk = 2:height(statskoin)
    text(statskoin.Centroid(kk,1)-20, statskoin.Centroid(kk,2)-10,'P');
    text(statskoin.Centroid(kk,1)-20, statskoin.Centroid(kk,2)+10,'L');
    text(statskoin.Centroid(kk,1), statskoin.Centroid(kk,2)-10,num2str(panjangkoin/58.7));
    
    text(statskoin.Centroid(kk,1), statskoin.Centroid(kk,2)+10,num2str(lebarkoin/58.7));
end
%------------------------------------------------------------------------------------------------------------------------
 
%cetak tulisan di setiap biji jagung
%------------------------------------------------------------------------------------------------------------------------
for kk = 1:length(stats) 
    tempx=stats(kk).Area;
    
    if tempx>50 && tempx<105 && stats(kk).BoundingBox(1)<950 
    text(stats(kk).Centroid(1)-5, stats(kk).Centroid(2)-10,'P');
    text(stats(kk).Centroid(1)-5, stats(kk).Centroid(2)+10,'L');
    text(stats(kk).Centroid(1), stats(kk).Centroid(2)-10,num2str(stats(kk).BoundingBox(3)/58.7));
    text(stats(kk).Centroid(1), stats(kk).Centroid(2)+10,num2str(stats(kk).BoundingBox(4)/58.7));
    end
end
%------------------------------------------------------------------------------------------------------------------------