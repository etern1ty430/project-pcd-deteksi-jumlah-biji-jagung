%baca inputan gambar 
%------------------------------------------------------------------------------------------------------------------------
clc;
clear;
%masukkan inputan
inputpanjang=0.4;
inputlebar=0.4;
inputbiji=100;
i=imread('im1.jpg');
bw1=rgb2gray(i);
%------------------------------------------------------------------------------------------------------------------------

%membersihkan noise dan melakukan edge detection
%------------------------------------------------------------------------------------------------------------------------
bersih=imfilter(bw1,0.5);
bersih=imgaussfilt(bersih);
bersih=medfilt2(bersih);
sisi=edge(bersih,'log');
%------------------------------------------------------------------------------------------------------------------------

%menghilangkan sisi jagung agar mempermudah proses bounding box 
%------------------------------------------------------------------------------------------------------------------------
se90 = strel('line',3,90);
se0 = strel('line',3,0);
dilasi=imdilate(sisi,[se0 se90]);
isi=imfill(dilasi,'holes');
rangka=bwperim(isi);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
rangka=imdilate(rangka,[se0 se90]);
hasil=sisi-rangka;
%------------------------------------------------------------------------------------------------------------------------

%cetak bounding box untuk jagung
%------------------------------------------------------------------------------------------------------------------------
imshow(i);
hasil=imbinarize(hasil);
stats=regionprops(hasil);

for k = 1 : length(stats)
  temp=stats(k).Area;
  if temp>50 && temp<105 && stats(k).BoundingBox(1)<950 
  thisBB = stats(k).BoundingBox;
  if thisBB(3)/58.7>inputpanjang && thisBB(4)/58.7>inputlebar
    rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],'EdgeColor','g','LineWidth',1 )
  else
    rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],'EdgeColor','r','LineWidth',1 )
  end   
  end
end
%------------------------------------------------------------------------------------------------------------------------

%properti coin dan gambar bounding box untuk coin
%------------------------------------------------------------------------------------------------------------------------
koin=rgb2gray(i);
sisikoin=edge(koin,'sobel');
sisikoin=imdilate(sisikoin,[se0 se90]);
sisikoin=imfill(sisikoin,'holes');
se=strel('diamond',4);
sisikoin=imerode(sisikoin,se);
sisikoin=imdilate(sisikoin,se);

statskoin=regionprops(sisikoin);
panjangkoin=0;
lebarkoin=0;
for k = 1 : length(statskoin)
  temp=statskoin(k).Area;
  if statskoin(k).BoundingBox(3)<200
  thisBB = statskoin(k).BoundingBox;
  rectangle('Position', [thisBB(1),thisBB(2),thisBB(3),thisBB(4)],'EdgeColor','r','LineWidth',1 )
  panjangkoin=thisBB(3);
  lebarkoin=thisBB(4);
  end
end
%------------------------------------------------------------------------------------------------------------------------

%cetak tulisan di setiap di coin
%------------------------------------------------------------------------------------------------------------------------
statskoin = struct2table(statskoin);
for kk = 2:height(statskoin)
    text(statskoin.Centroid(kk,1)-20, statskoin.Centroid(kk,2)-10,'P');
    text(statskoin.Centroid(kk,1)-20, statskoin.Centroid(kk,2)+10,'L');
    text(statskoin.Centroid(kk,1), statskoin.Centroid(kk,2)-10,num2str(panjangkoin/58.7));
    
    text(statskoin.Centroid(kk,1), statskoin.Centroid(kk,2)+10,num2str(lebarkoin/58.7));
end
%------------------------------------------------------------------------------------------------------------------------
 
%cetak tulisan di setiap biji jagung
%------------------------------------------------------------------------------------------------------------------------
ctr=0;
for kk = 1:length(stats) 
    tempx=stats(kk).Area;
    
    if tempx>50 && tempx<105 && stats(kk).BoundingBox(1)<950 
    ctr=ctr+1;
    text(stats(kk).Centroid(1)-5, stats(kk).Centroid(2)-10,'P');
    text(stats(kk).Centroid(1)-5, stats(kk).Centroid(2)+10,'L');
    text(stats(kk).Centroid(1), stats(kk).Centroid(2)-10,num2str(stats(kk).BoundingBox(3)/58.7));
    text(stats(kk).Centroid(1), stats(kk).Centroid(2)+10,num2str(stats(kk).BoundingBox(4)/58.7));
    end
end
%cetak informasi di console
disp("Jumlah Bijinya Adalah : "+ctr);
if ctr>inputbiji
    disp("Memenuhi syarat");
else
    disp("Tidak memenuhi syarat");
end
%------------------------------------------------------------------------------------------------------------------------

